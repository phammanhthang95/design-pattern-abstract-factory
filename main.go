package main

import (
	"desgin-pattern-abstract-factory/pkg"
	"fmt"
)

func main() {
	adidasFactory := pkg.GetSportsFactory("adidas")
	adidasShoe := adidasFactory.MakeShoe()
	printShoeDetail(adidasShoe)
	adidasShort := adidasFactory.MakeShort()
	printShortDetail(adidasShort)

	nikeFactory := pkg.GetSportsFactory("nike")
	nikeShoe := nikeFactory.MakeShoe()
	printShoeDetail(nikeShoe)
	nikeShort := nikeFactory.MakeShort()
	printShortDetail(nikeShort)
}

func printShoeDetail(s pkg.IShoe) {
	fmt.Printf("Logo: %s\n", s.GetLogo())
	fmt.Printf("Size: %d\n", s.GetSize())
}

func printShortDetail(s pkg.IShoe) {
	fmt.Printf("Logo: %s\n", s.GetLogo())
	fmt.Printf("Size: %d\n", s.GetSize())
}