package pkg

type Adidas struct {
}

func (a *Adidas) MakeShoe() IShoe {
	return &adidasShoe{
		shoe: shoe{
			logo: "adidas shoe",
			size: 18,
		},
	}
}

func (a *Adidas) MakeShort() IShort {
	return &adidasShort{
		short: short {
			logo: "adidas short",
			size: 18,
		},
	}
}