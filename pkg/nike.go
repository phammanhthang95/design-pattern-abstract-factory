package pkg

type Nike struct {
}

func (a *Nike) MakeShoe() IShoe {
	return &nikeShoe{
		shoe: shoe{
			logo: "nike shoe",
			size: 14,
		},
	}
}

func (a *Nike) MakeShort() IShort {
	return &nikeShort{
		short: short {
			logo: "nike short",
			size: 14,
		},
	}
}